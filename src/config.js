import { config } from "dotenv";
config();

export const database = {
  connectionLimit: 10,
  host: process.env.MYSQL_HOST || "192.168.0.23",
  port: process.env.MAYSQL_PORT || "3306", 
  user: process.env.MYSQL_USER || "root",
  password: process.env.MYSQL_PASSWORD || "1234",
  database: process.env.MYSQL_NAME || "dblinks",
};

export const port = process.env.PORT || 4000;
