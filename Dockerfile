FROM debian:latest
RUN apt-get update -y
RUN apt-get install nodejs npm -y
COPY src/ /app/
WORKDIR /app
EXPOSE 4000
CMD node app.js
